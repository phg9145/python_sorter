# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 20:17:15 2020

@author: Philipp von Perponcher
"""


from Player import Player
from Switch import Switch
from BenchSwitch import BenchSwitch
from Team import Team
from Bench import Bench
import random as rand
import sys
from datetime import datetime


bench = Bench()
all_teams = {}
all_players = {}
available_players = []
available_players_roles = {}
final_message = ""


outputTextFile = True






def printAllTeams():
    global all_teams
    
    for t in all_teams:
        # t.printTeam()
        all_teams[t].printTeam()


def printAllTeamsShort():
    global all_teams
    
    for t in all_teams:
        # t.printTeam()
        all_teams[t].printTeamShort()
    



"""
Getting all players within teams
"""
def getPlayersIntoTeams():
    global all_teams
    global all_players
    
    # opening the file 'players_available.txt' and reading all the lines, saving them in the list aL
    with open("players_teamcalc.txt", 'r') as f:
        aL = f.readlines()
    
    allLines = []
    # for every line in aL, the line is stripped of any whitespaces (rstrip()) and appended to the List allLines
    # if it doesn't have a # in the first character (Used for manually commenting out players from playing)
    for line in aL:
        if line[0] != '#' and len(line.rstrip()) != 0:
            allLines.append(line.rstrip())
    
    for line in allLines:
        print(line)
    
    
    currentTeam = ""
    for line in allLines:
        line_split = line.split(" ")
        if line_split[0] == "team":
            teamname = line_split[1]
            t = Team(teamname)
            all_teams[teamname] = t
            currentTeam = t
        else:
            pickPlayer(currentTeam, all_players[line_split[1]], line_split[0])
    
    
    
    # print()


"""
The most basic function of this program: "Give" a player p to a team t in role r (r in {'t','d','s'})

@param t: Team
@param p: Player
@param r: Role (in {'t','d','s'})
"""
def pickPlayer(t, p, r):
    
    # Check if the given role r is 't', 'd' or 's' to prevent errors later on
    if r not in ('t', 'd', 's'):
        print("Error: " + str(r) + " is not a role!")
        return False

    # Check with the team if the player can be picked for the role
    # If that is True, the player will be removed from both player lists and this function returns True
    if t.givePlayer(p, r) == True:
        print("Pick successful!")
        return True
    else:
        print("Error: Player can't be picked for this team (role full)")
        return False


"""
Getting all players from the players.txt file and saving them into the global list all_players
"""
def getPlayers():
    global all_players
    
    # opening the file 'players_available.txt' and reading all the lines, saving them in the list aL
    with open("players_taw.txt", 'r') as f:
        aL = f.readlines()
    
    allLines = []
    # for every line in aL, the line is stripped of any whitespaces (rstrip()) and appended to the List allLines
    # if it doesn't have a # in the first character (Used for manually commenting out players from playing)
    for line in aL:
        if line[0] != '#' and len(line.rstrip()) != 0:
            allLines.append(line.rstrip())
    
    # After having read all lines of the players-file, now the lines need to be split up and converted into players
    
    allRoles = ['t','d','s']
    
    for line in allLines:
        print(line)
        line_split = line.split(' - ')
        
        p = Player(line_split[0], int(line_split[1]), int(line_split[2]), int(line_split[3]), allRoles)
        
        all_players[line_split[0]] = p
    
    # Adding a null-player
    p = Player("NullPlayer", 0, 0, 0, allRoles)
    all_players["NullPlayer"] = (p)
    
    # Now, all players should be saved in the global variable all_players.
    # Let's print all their names just to be sure
    
    # for p in available_players:
    #     print(p.getName())
    
    print("All players read!")
    # print()

"""
Before filling them, the teams have to be created first
"""
def createTeams(all_names):
    global all_teams
    
    # Create the teams and append it to the global variable all_teams
    for i in range(len(all_names)):
        name = all_names[i]
        t = Team(name)
        all_teams[name] = t


"""
Central function
"""
def run():
    
    getPlayers()
    
    getPlayersIntoTeams()
    
    printAllTeams()
    
    printAllTeamsShort()
    
    
    #############
        

if __name__== "__main__":
    print("Running")
    
    run()
    print("=============")
    print("Done")