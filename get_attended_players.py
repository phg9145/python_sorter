# -*- coding: utf-8 -*-
"""
Created on Sun Aug 30 16:44:11 2020

@author: Philipp
"""

def getPlayers():
    
    print("getting Players")
    
    with open("attendance.txt", 'r') as f:
        aL = f.readlines()
    
    with open("players_taw.txt", 'r') as f:
        pL = f.readlines()
    
    allPlayers = {}
    unavailable = []
    
    for line in pL:
        line_sp = line.split(" ")
        if line_sp[0] != '#':
            allPlayers[line_sp[0]] = line
        else:
            unavailable.append(line_sp[1])
    
    
    
    plKeys = list(allPlayers.keys())
    
    print(aL)
    
    print("Keys: " + str(plKeys))
    
    allLines = []
    aL.pop(0)
    for line in aL:
        if 'attended' in line:
        # if 'Excused' not in line and '---' not in line:
            line_sp = line.split("\t")
            player = line_sp[0]
            print("Player: " + player)
            if player in unavailable:
                continue
            if player in plKeys:
                allLines.append(allPlayers[player])
                print("Line appended: " + allPlayers[player])
            else:
                allLines.append(player)
                print("Player appended: " + player)
            
    
    with open("players_available.txt", 'w') as f:
        for l in allLines:
            f.write(l.rstrip() + "\n")
        
        f.write("# Tank\n\n")
        f.write("# DPS\n\n")
        f.write("# Support\n\n")
        f.write("# Flex")
    
    # Read all lines of the players-file, now the lines need to be split and assigned to the players



if __name__== "__main__":
    print("getting Players")
    getPlayers()