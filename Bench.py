# -*- coding: utf-8 -*-
"""
Created on Sat Aug  1 20:36:21 2020

@author: Philipp
"""

import statistics as stat

class Bench:
    
    def __init__(self):
        self.name = "Bench"
        self.players_tank = []
        self.players_dps = []
        self.players_supp = []
    
    
    def getName(self):
        return self.name
    
    def getNumberTank(self):
        return len(self.players_tank)
    
    def getNumberDPS(self):
        return len(self.players_dps)
    
    def getNumberSupp(self):
        return len(self.players_supp)
    
    def getPlayersTank(self):
        return self.players_tank
    
    def getPlayersDPS(self):
        return self.players_dps
    
    def getPlayersSupp(self):
        return self.players_supp
    
    def getAllPlayers(self):
        full = self.players_tank.copy()
        full.extend(self.players_dps)
        full.extend(self.players_supp)
        return full
    
    def getPlayers(self, role):
        if role == 't':
            return self.getPlayersTank()
        
        elif role == 'd':
            return self.getPlayersDPS()
        
        elif role == 's':
            return self.getPlayersSupp()
        else:
            print("Error in getPlayers: " + str(role) + " is not a role!")
            return False
    
    def getAvgTank(self):
        val = []
        
        # If no tanks are set atm, the function will return nothing
        if len(self.players_tank) == 0:
            return
        
        for p in self.players_tank:
            val.append(p.getTank())
        
        return stat.mean(val)
    
    def getAvgDPS(self):
        val = []
        
        # If no dps are set atm, the function will return nothing
        if len(self.players_dps) == 0:
            return
        
        for p in self.players_dps:
            val.append(p.getDPS())
        
        return stat.mean(val)
    
    def getAvgSupp(self):
        val = []
        
        # If no supports are set atm, the function will return nothing
        if len(self.players_supp) == 0:
            return
        
        for p in self.players_supp:
            val.append(p.getSupp())
        
        return stat.mean(val)
    
    def getAvgTotal(self):
        val = []
        
        # If no supports are set atm, the function will return nothing
        if len(self.players_tank) == 0 and len(self.players_dps) == 0 and len(self.players_supp) == 0:
            return -1
        
        for p in self.players_tank:
            val.append(p.getTank())
        
        for p in self.players_dps:
            val.append(p.getDPS())
        
        for p in self.players_supp:
            val.append(p.getSupp())
        
        return stat.mean(val)
    
    def giveTank(self, pl):
        self.players_tank.append(pl)
        return True
    
    def giveDPS(self, pl):
        self.players_dps.append(pl)
        return True
    
    def giveSupp(self, pl):
        self.players_supp.append(pl)
        return True
    
    def givePlayer(self, player, role):
        if role == 't':
            return self.giveTank(player)
        
        elif role == 'd':
            return self.giveDPS(player)
        
        elif role == 's':
            return self.giveSupp(player)
        else:
            print("Error in givePlayer: " + str(role) + " is not a role!")
            return False
    
        
    
    def removePlayer(self, player, role):
        if role == 't':
            try:
                self.players_tank.remove(player)
            except:
                return False
        
        elif role == 'd':
            try:
                self.players_dps.remove(player)
            except:
                return False
        elif role == 's':
            try:
                self.players_supp.remove(player)
            except:
                return False
        else:
            print("Error: " + str(role) + " is not a role!")
            return False
        
        return True

    def removePlayerNoRole(self, player):
        
        allLists = (self.players_tank, self.players_dps, self.players_supp)
        for role_list in allLists:
            if player in role_list:
                role_list.remove(player)
                return True
        
        return False

    def printTeamRoles(self):
        print("-------------")
        print("Team Name: " + self.name)
        print("Team average: " + str(round(self.getAvgTotal(), 3)))
        print()
        
#        print("All tanks:")
        for p in self.players_tank:
            st = p.getName() + " - " + str(p.getTank())
            print("Tank: " + st)
            
#        print("All DPS:")
        for p in self.players_dps:
            st = p.getName() + " - " + str(p.getDPS())
            print("DPS: " + st)
            
#        print("All supports:")
        for p in self.players_supp:
            st = p.getName() + " - " + str(p.getSupp())
            print("Support: " + st)
        
        print("-------------")

    def printTeam(self):
        print("-------------")
        print("Team Name: " + self.name)
        # print("Team average: " + str(round(self.getAvgTotal(), 3)))
        print()
        
        pl = self.getAllPlayers()
#        print("All tanks:")
        
        for p in pl:
            print(p.toString())
        
        print("-------------")