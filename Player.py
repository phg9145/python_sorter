# -*- coding: utf-8 -*-
"""
Created on Sat Aug  1 20:46:30 2020

@author: Philipp
"""



class Player:
    
    def __init__(self, name, tank, dps, supp, roles):
        self.name = str(name)
        self.tank = int(tank)
        self.dps = int(dps)
        self.supp = int(supp)
        self.roles = roles
        
    def getName(self):
        return self.name
    
    def getTank(self):
        return self.tank
    
    def getDPS(self):
        return self.dps
    
    def getSupp(self):
        return self.supp
    
    def getRoles(self):
        return self.roles
    
    def getSR(self, role):
        if role == 't':
            return self.getTank()
        
        elif role == 'd':
            return self.getDPS()
        
        elif role == 's':
            return self.getSupp()
        else:
            print("Error in getSR: " + str(role) + " is not a role!")
            return -1
    
    def toString(self):
        return self.name + ": " + str(self.tank) + " - " + str(self.dps) + " - " + str(self.supp) + " - " + str(self.roles)