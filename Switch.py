# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 17:09:45 2020

@author: Philipp
"""

from Team import Team
from Player import Player


class Switch:
    
    def __init__(self, team1, role1, player1, team2, role2, player2):
        self.team1 = team1
        self.role1 = role1
        self.player1 = player1
        
        self.team2 = team2
        self.role2 = role2
        self.player2 = player2
        
        self.calcSRChange()
        
        self.role1Name = ''
        if role1 == 't':
            self.role1Name = 'Tank'
        elif role1 == 'd':
            self.role1Name = 'DPS'
        elif role1 == 's':
            self.role1Name = 'Support'
        else:
            self.role1Name = 'Error with Role 1'
        
        self.role2Name = ''
        if role2 == 't':
            self.role2Name = 'Tank'
        elif role2 == 'd':
            self.role2Name = 'DPS'
        elif role2 == 's':
            self.role2Name = 'Support'
        else:
            self.role2Name = 'Error with Role 2'
        
        
        self.calcSwitchPossible()
        #print("switchPossible was calculated, it's " + str(self.switchPossible))

    
    def getID(self):
        val_str = ""
        plList = [self.player1.getName(), self.player2.getName()]
        # print(plList)
        plList.sort()
        # print(plList)
        for p in plList:
            val_str += p
        
        val_str += self.role1
        val_str += self.role2
        
        return val_str
    
    def getSRChange(self):
        return self.SRChange
    
    def getSwitchPossible(self):
        return self.switchPossible
    """
    Function to calculate, which SR difference it would make if this switch was made
    @return: returns the difference of the new average SRs.
    Example: A switch, where team1 gets +10SR and team2 -15SR returns +25
    """
        
    def calcSRChange(self):
        oldTeam1 = self.team1.getAvgTotal() * 6
        oldTeam2 = self.team2.getAvgTotal() * 6
        
        team1_without_p1 = oldTeam1 - self.player1.getSR(self.role1)
        team2_without_p2 = oldTeam2 - self.player2.getSR(self.role2)
        
        team1_after_switch = team1_without_p1 + self.player2.getSR(self.role1)
        team2_after_switch = team2_without_p2 + self.player1.getSR(self.role2)
        
        newTeam1 = (team1_after_switch - oldTeam1) / 6
        newTeam2 = (team2_after_switch - oldTeam2) / 6
        
        self.SRChange = newTeam1 - newTeam2

    def makeSwitch(self):
        print("In makeSwitch")
        switch_1_success = self.team1.removePlayer(self.player1, self.role1)
        switch_2_success = self.team2.removePlayer(self.player2, self.role2)
        
        print("switch_1_success: " + str(switch_1_success))
        print("switch_2_success: " + str(switch_2_success))
        
        if switch_1_success == False:
            self.team1.givePlayer(self.player1, self.role1)
        
        if switch_2_success == False:
            self.team2.givePlayer(self.player2, self.role2)
        
        if switch_1_success and switch_2_success:
            self.team1.givePlayer(self.player2, self.role1)
            self.team2.givePlayer(self.player1, self.role2)
        else:
            print("AND-operation not successful")
            return False
        
        print()
        print("Switch was made:")
        print(self.player1.getName() + " was transfered to " + self.team2.getName() + ", new Role: " + self.role2Name)
        print(self.player2.getName() + " was transfered to " + self.team1.getName() + ", new Role: " + self.role1Name)
        print("The SR Change by this Switch was " + str(round(self.SRChange, 3)))
        print()
        return True
    
    def toShortString(self):
        return self.player1.getName() + " <--> " + self.player2.getName()

    def calcSwitchPossible(self):
# =============================================================================
#         print("-----")
#         print("In a switch")
#         print("Role 1: " + self.role1Name)
#         print("Role 2: " + self.role2Name)
#         print("Player 1: " + self.player1.toString())
#         print("Player 2: " + self.player2.toString())
#         print()
#         print("Player 2 roles: " + str(self.player2.getRoles()))
#         print("Role 1: " + self.role1)
#         print("Role 1 in player 2's roles? " + str(self.role1 in self.player2.getRoles()))
#         print()
#         print("Player 1 roles: " + str(self.player1.getRoles()))
#         print("Role 2: " + self.role2)
#         print("Role 2 in player 1's roles? " + str(self.role2 in self.player1.getRoles()))
# =============================================================================
        self.switchPossible = (self.role1 in self.player2.getRoles()) & (self.role2 in self.player1.getRoles())
        
#        print("Switch possible? " + str(self.switchPossible))
