# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 20:17:15 2020

@author: Philipp von Perponcher
"""


from Player import Player
from Switch import Switch
from BenchSwitch import BenchSwitch
from Team import Team
from Bench import Bench
import random as rand
import sys
from datetime import datetime


bench = Bench()
all_teams = []
all_players = []
available_players = []
available_players_roles = {}
final_message = ""


outputTextFile = True



"""
The most basic function of this program: "Give" a player p to a team t in role r (r in {'t','d','s'})

@param t: Team
@param p: Player
@param r: Role (in {'t','d','s'})
"""
def pickPlayer(t, p, r):
    global available_players_roles
    
    
    # If p is not available (already in a team or not playing), we return False
    if p not in available_players_roles[r]:
        print("Error: Player not available for picking")
        return False
    
    # Check if the given role r is 't', 'd' or 's' to prevent errors later on
    if r not in ('t', 'd', 's'):
        print("Error: " + str(r) + " is not a role!")
        return False

    # Check with the team if the player can be picked for the role
    # If that is True, the player will be removed from both player lists and this function returns True
    if t.givePlayer(p, r) == True:
        available_players_roles[r].remove(p)
        print("Pick successful!")
        return True
    else:
        print("Error: Player can't be picked for this team (role full)")
        return False


"""
"Give" a player p to a team t in role r (r in {'t','d','s'}), take him from the bench though

@param t: Team
@param p: Player
@param r: Role (in {'t','d','s'})
"""
def pickPlayerFromBench(t, p, r):
    global bench    
    
    # If p is not on the bench, we return False
    if p not in bench.getAllPlayers():
        print("Error: Player not available for picking")
        return False
    
    # Check if the given role r is 't', 'd' or 's' to prevent errors later on
    if r not in ('t', 'd', 's'):
        print("Error: " + str(r) + " is not a role!")
        return False

    # Check with the team if the player can be picked for the role
    # If that is True, the player will be removed from both player lists and this function returns True
    if t.givePlayer(p, r) == True:
        print(bench.removePlayerNoRole(p))
        print("Pick successful!")
        return True
    else:
        print("Error: Player can't be picked for this team (role full)")
        return False


    

# Functions for good printing of the respective object

def printAllPlayers():
    global all_players
    
    print("#############")
    print("All players that are up for being picked:\n")
    
    for p in all_players:
        print(p.toString())
    
    print("#############")

def printAvailablePlayers():
    global available_players_roles
    
    print("#############")
    print("All players that are pickable:\n")
    
    for r in ('t', 'd', 's'):
        for p in available_players_roles[r]:
            print(p.toString())
    
    print("#############")

def printAvailableTanks():
    global available_players_roles
    
    print("#############")
    print("All tanks that are still pickable:\n")
    
    for p in available_players_roles['t']:
        print(p.toString())
    
    print("#############")

def printAvailableDPS():
    global available_players_roles
    
    print("#############")
    print("All DPS that are still pickable:\n")
    
    for p in available_players_roles['d']:
        print(p.toString())
    
    print("#############")

def printAvailableSupports():
    global available_players_roles
    
    print("#############")
    print("All supports that are still pickable:\n")
    
    for p in available_players_roles['s']:
        print(p.toString())
    
    print("#############")


def printAllTeams():
    global all_teams
    global bench
    
    for t in all_teams:
        t.printTeam()
    
    bench.printTeam()



# Now, the players have to be read from the players.txt file and assigned to Teams
"""
Getting all players from the players.txt file and saving them into the global list all_players
"""
def getPlayers_no_pref_role():
    global all_players
    global available_players_roles
    
    
    # every role gets its own list of available players
    available_players_roles['t'] = []
    available_players_roles['d'] = []
    available_players_roles['s'] = []
    
    # opening the file 'players_available.txt' and reading all the lines, saving them in the list aL
    with open("players_available.txt", 'r') as f:
        aL = f.readlines()

    
    allLines = []
    # for every line in aL, the line is stripped of any whitespaces (rstrip()) and appended to the List allLines
    # if it doesn't have a # in the first character (Used for manually commenting out players from playing)
    for line in aL:
        if line[0] != '#':
            allLines.append(line.rstrip())
    
    number_of_players = len(allLines)
    numberRoles = {}
    numberRoles['t'] = 0
    numberRoles['d'] = 0
    numberRoles['s'] = 0
    
    # After having read all lines of the players-file, now the lines need to be split up and converted into players
    
    flex_players_lines = []
    
    for line in allLines:
        line_split = line.split(' - ')
        roles = []
        roles[:] = line_split[4]
        
        # If they only play one role, they are appended to their respective list directly
        if len(roles) == 1:
            pref_role = roles[0]
            numberRoles[pref_role] += 1
            p = Player(line_split[0], int(line_split[1]), int(line_split[2]), int(line_split[3]), roles)
            available_players_roles[pref_role].append(p)
            all_players.append(p)
        else:
            flex_players_lines.append(line)
    
    print("Number tank: " + str(numberRoles['t']))
    print("Number DPS: " + str(numberRoles['d']))
    print("Number support: " + str(numberRoles['s']))
    print("Number left: " + str(len(flex_players_lines)))
    print("Total number: " + str(number_of_players))
    
    # Alright. Now, we have all the one-role-players done, we still need to give the other players their preferred roles
    # TODO
    
    
    
    
    
    # Now, all players should be saved in the global variable all_players.
    # Let's print all their names just to be sure
    
    # for p in available_players:
    #     print(p.getName())
    
    print("All players read!")
    # print()

"""
Getting all players from the players.txt file and saving them into the global list all_players
"""
def getPlayers_with_pref_role():
    global all_players
    global available_players_roles
    
    
    # every role gets its own list of available players
    available_players_roles['t'] = []
    available_players_roles['d'] = []
    available_players_roles['s'] = []
    
    # opening the file 'players_available.txt' and reading all the lines, saving them in the list aL
    with open("players_available.txt", 'r') as f:
        aL = f.readlines()
    
    allLines = []
    # for every line in aL, the line is stripped of any whitespaces (rstrip()) and appended to the List allLines
    # if it doesn't have a # in the first character (Used for manually commenting out players from playing)
    for line in aL:
        if line[0] != '#':
            allLines.append(line.rstrip())
    
    # After having read all lines of the players-file, now the lines need to be split up and converted into players
    
    for line in allLines:
        line_split = line.split(' - ')
        
        #allRoles = ['t','d','s']
        roles = []
        print(line_split)
        roles[:] = line_split[4]
        # print(line_split[0])
        # print(len(line_split))
        # print()
        # print(line_split)
        pref_role = line_split[5]
        p = Player(line_split[0], int(line_split[1]), int(line_split[2]), int(line_split[3]), roles)
        
        # print(line_split[0] + " has the roles " + str(roles))
        
        available_players_roles[pref_role].append(p)
        
        #for r in roles:
        #    available_players_roles[r].append(p)
        
        all_players.append(p)
    
    # Now, all players should be saved in the global variable all_players.
    # Let's print all their names just to be sure
    
    # for p in available_players:
    #     print(p.getName())
    
    print("All players read!")
    # print()

"""
Before filling them, the teams have to be created first

@param numberOfTeams: the number of teams to be created
"""
def createTeams(numberOfTeams):
    global all_teams
    
    # First, get all the team names
    all_names = ['Fighting Foxes', ' Dancing Dragons', 'Iron Wolves', 'Strike Team 404', 'Knightly Kittens', 'Suwon Tigers']
    # all_names = ['Naughty Tomatoes', 'Strike Team 404', 'Suwon Tigers', 'Lucky Runners', 'Raccoon Ravagers', 'Pesky Pachimaris', 'Team 7', 'Team 8']
    # all_names = ['Suwon Tigers', 'Lucky Runners', 'Raccoon Ravagers', 'Pesky Pachimaris', 'Team 7', 'Team 8']
    # all_names = ['Strike Team 404', 'Pesky Pachimaris', 'Team 7', 'Team 8']
    
    # Create the teams and append it to the global variable all_teams
    for i in range(numberOfTeams):
        name = all_names[i]
        t = Team(name)
        all_teams.append(t)


"""
Help Function for filling the teams: first 6 --> team1 etc.
"""
def fill_teams_0():
    global all_teams
    global available_players_roles
    
    for t in all_teams:
        for i in range(2):
            try:
                pickPlayer(t, available_players_roles['t'][0], 't')
            except:
                print()
        for i in range(2):
            try:
                pickPlayer(t, available_players_roles['d'][0], 'd')
            except:
                print()
        for i in range(2):
            try:
                pickPlayer(t, available_players_roles['s'][0], 's')
            except:
                print()
    
    return 0

"""
1st Function of filling the teams: random, regarding roles
The try catch is needed in case there are not enough players available anymore
"""
def fill_teams_1():
    global all_teams
    
    for t in all_teams:
        t.printTeam()
        
        # try to pick 2 tanks
        try:
            tank_random = rand.sample(available_players_roles['t'], 2)
            for p in tank_random:
                pickPlayer(t, p, 't')
        except:
            print()
        
        # try to pick 2 DPS
        try:
            dps_random = rand.sample(available_players_roles['d'], 2)
            for p in dps_random:
                pickPlayer(t, p, 'd')
        except:
            print()
        
        # try to pick 2 Supports
        try:
            supp_random = rand.sample(available_players_roles['s'], 2)
            for p in supp_random:
                pickPlayer(t, p, 's')
        except:
            print()
        
        # Just some debug printing
        t.printTeam()
        printAvailableTanks()
        printAvailableDPS()
        printAvailableSupports()
    return 0

"""
Filling the bench with players
Every player still remaining after filling the teams gets added to the bench
"""
def fill_bench():
    global bench
    global available_players_roles
    
    printAvailablePlayers()
    
    for role in ('t', 'd', 's'):
        for i in range(len(available_players_roles[role])):
            bench.givePlayer(available_players_roles[role].pop(0), role)


"""
In case the roles are not perfectly filled, there might be gaps.
Here, those gaps will be tried to be filled
"""
def fill_the_rest():
    global all_teams
    global bench
    
    count = 1
    
    while count != 0:
        count = 0
        for t in all_teams:
            # let's see if t has a gap
            gap = t.getGaps()
            if gap == "":
                print(t.getName() + " is a full team:")
                t.printTeam()
                continue
            
            # we know that this team still has a gap
            print(t.getName() + " still has gaps:")
            t.printTeam()
            
            for g in list(gap):
                print("Gap: " + g)
                c = 0
                bench_team = bench.getAllPlayers()
                # print("Bench: " + str(bench_team))
                
                for pl in bench_team:
                    c += 1
                    print(c)
                    print("Trying to fill the gap " + g + " on team " + t.getName() + " with " + pl.toString())
                    # Does pl even play the role that we want?
                    if g in pl.getRoles():
                        if pickPlayerFromBench(t, pl, g):
                            print("Picking successful!")
                            count += 1
                        else:
                            print("Picking not successful :(")
                    else:
                        print(pl.getName() + " doesn't play this role, keep going")
            
            if count != 0:
                continue
    
            # Alright. Now, we have no players with this role left on the bench anymore
            # What we can do is try to switch a player from another role to the gap to then fill up the new gap
            
            gaps = t.getGaps()
            if gaps == "":
                print(t.getName() + " is full:")
                t.printTeam()
                continue
            
            print("Going to a 2nd round")
            bench_team = bench.getAllPlayers()
            
            # For each gap that this team still has
            for gap in gaps:
                # Going through every player that's still on the bench
                for pl_bench in bench_team:
                    roles = pl_bench.getRoles()
                    
                    # Getting the roles from the bench player
                    for role in roles:
                        if role == gap:
                            continue
                        # Now we need to find a player on the team that can the gap role but is currently in a different role
                        players_team_switch_role = t.getPlayers(role)
                        
                        for pl_team in players_team_switch_role:
                            print("Gap: " + gap + ", role inside team: " + role + ", player inside team:")
                            print(pl_team.toString())
                            if gap in pl_team.getRoles():
                                # Now, we have a player on the team with the role that we need (for the gap),
                                # currently playing in a role that a bench player can also fill.
                                # Let's first switch the player from role to gap, then pick the bench player
                                if t.movePlayer(pl_team, role, gap) == True:
                                    pickPlayerFromBench(t, pl_bench, role)
                                    count += 1
                                    break
                                else:
                                    print("Something went wrong. movePlayer returned False")



"""
Taking pairs of teams and balancing them with each other
"""
def balance_teams_1():
    global all_teams
    global final_message
    
    # Not very elegant but creating the final message which will be assigned in the end of the function
    final_message = ""
    
    
    teams = {}
    for t in all_teams:
        teams[t.getName()] = t
    
    team_keys = list(teams.keys())
    
    for i in range(int(len(teams)/2)):
        t1 = teams[team_keys.pop(0)]
        t2 = teams[team_keys.pop(0)]
        make_balance_1(t1, t2)
        
        final_message += "Matchup " + str(i+1) + ": " + t1.getName() + " - " + t2.getName() + ", difference: " + str(round(t1.getAvgTotal() - t2.getAvgTotal(), 3)) + "\n"
    
    
    if len(team_keys) != 0:
        final_message += "Leftover team: " + teams[team_keys[0]].getName()


"""
Trying to see what every possible balance would bring, also regarding roles (s.getSwitchPossible())
"""
def make_balance_1(team1, team2):
    global bench
    
    # Just some Debug output
    print("########################################")
    print("########################################")
    print("Balancing " + team1.getName() + " and " + team2.getName() + "...")
    
    # avg1 = average team 1
    # avg2 = average team 2
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    
    # The starting difference is calculated for the final Debug output (how much did we get better?)
    startdiff = round(avg1 - avg2, 3)
    
    
    # Just so I don't have to make a While True
    stillSwitchesPossible = True
    
    allSwitches = []
    
    # Just to count how many switches we made, just for nice Debug printing purposes
    counter = 0
    
    
    while stillSwitchesPossible:
        
        # We have to get the averages again each time as it will change with the switches
        avg1 = team1.getAvgTotal()
        avg2 = team2.getAvgTotal()
        diff = round(avg1 - avg2, 3)
        
        # Creating the map for saving the keys
        all_switches = {}
        roles = {'t', 'd', 's'}
        
        # Going through every role on each time
        for role1 in roles:
            for role2 in roles:
                players_team_1 = team1.getPlayers(role1)
                players_team_2 = team2.getPlayers(role2)
                
                # for every player in that role
                for p1 in players_team_1:
                    for p2 in players_team_2:
                    
                        # First, create the switch
                        s = Switch(team1, role1, p1, team2, role2, p2)
                    
                        # Then, get the SR change made by the switch
                        change = s.getSRChange()
                    
                        # Then, get the SR difference after the switch. This is the key that we will use to save the switch in all_switches.
                        # By using this key, we can later on easily sort the keys low --> high and therefore get the best switch
                        k = abs(diff + change)
                    
                        # The switch makes sense if:
                        # the new avg is lower or equal and the Switch is possible and this switch hasn't been done yet
                        # A switch changing nothing is only allowed once, maybe this actually changes something
                        if abs(k) <= abs(diff) and change != 0 and s.getSwitchPossible() and s.getID() not in allSwitches:
                            all_switches[k] = s
        
        # Maybe a switch with the bench could help?
        for role in roles:
            players_team_1 = team1.getPlayers(role)
            players_team_2 = team2.getPlayers(role)
            players_bench = bench.getAllPlayers()
            
            # Try to make switches: team 1 <--> bench
            for p1 in players_team_1:
                for p_b in players_bench:
                    
                    # First, create the switch
                    s = BenchSwitch(team1, role, p1, bench, p_b)
                    
                    # Then, get the SR change made by the switch
                    change = s.getSRChange()
                    
                    # Then, get the SR difference after the switch. This is the key that we will use to save the switch in all_switches.
                    # By using this key, we can later on easily sort the keys low --> high and therefore get the best switch
                    k = abs(diff + change)
                    
                    # The switch makes sense if:
                    # the new avg is lower or equal and the Switch is possible and this switch hasn't been done yet
                    # A switch changing nothing is only allowed once, maybe this actually changes something
                    if abs(k) <= abs(diff) and change != 0 and s.getSwitchPossible() and s.getID() not in allSwitches:
                        all_switches[k] = s
            
            # Try to make switches: team 2 <--> bench
            for p2 in players_team_2:
                for p_b in players_bench:
                    
                    # First, create the switch
                    s = BenchSwitch(team2, role, p2, bench, p_b)
                    
                    # Then, get the SR change made by the switch
                    # As it's team 2, the change has to be inverted
                    change = (-1) * s.getSRChange()
                    
                    # Then, get the SR difference after the switch. This is the key that we will use to save the switch in all_switches.
                    # By using this key, we can later on easily sort the keys low --> high and therefore get the best switch
                    k = abs(diff + change)
                    
                    # The switch makes sense if:
                    # the new avg is lower or equal and the Switch is possible and this switch hasn't been done yet
                    # A switch changing nothing is only allowed once, maybe this actually changes something
                    if abs(k) <= abs(diff) and change != 0 and s.getSwitchPossible() and s.getID() not in allSwitches:
                        all_switches[k] = s
        
        
        # Now, all possible switches are saved.
        # To sort them, we first need to get all the keys
        switch_keys = list(all_switches.keys())
        
        # Here, we can check if there are even any switches left. If not, we leave
        if len(switch_keys) == 0:
            stillSwitchesPossible = False
            break
        
        # To get the best switch, we sort the list from low to high
        # This is default in list.sort()
        list.sort(switch_keys)
        
        # Some debugging prints
        print()
        print("Current SR difference (team1 - team2): " + str(diff))
        print("There are " + str(len(switch_keys)) + " switch(es) available:")
        
        for tk in switch_keys:
            print(str(round(tk, 3)) + ": " + all_switches[tk].toShortString())
        
        # The best switch that we can make is now at the top
        best_switch = all_switches[switch_keys[0]]
        print("\nSwitching " + best_switch.toShortString())
        
        # So we make this switch
        success = best_switch.makeSwitch()
        print("Switch success: " + str(success))
        
        # And append it to the switches that we've already made. By this, we prevent switches going back and forth all the time
        allSwitches.append(best_switch.getID())
            
        # We then update the bench (just for debugging) and print it
        players_bench = bench.getAllPlayers()
        bench.printTeam()
        
        # Increment the counter by 1
        counter += 1
        
        # Then just a few functions to show how good this switch was
        avg1 = team1.getAvgTotal()
        avg2 = team2.getAvgTotal()
        diff = round(avg1 - avg2, 3)
        
        print("SR difference after the Switch: " + str(diff))
    
    
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    diff = round(avg1 - avg2, 3)
    
    print(str(counter) + " switch(es) done.")
    print("Starting difference: " + str(startdiff))
    print("Final SR difference (team1 - team2): " + str(diff) + "\n")



"""
Balancing only within the role
--> a tank player on team 1 can't be switched with a DPS on team 2
"""
def make_balance_2(team1, team2):
    print("########################################")
    print("########################################")
    print("Balancing " + team1.getName() + " and " + team2.getName() + "...")
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    startdiff = round(avg1 - avg2, 3)
    
    counter = 0
    
    stillSwitchesPossible = True
    # Try tank switches
    
    while stillSwitchesPossible:
        avg1 = team1.getAvgTotal()
        avg2 = team2.getAvgTotal()
        diff = round(avg1 - avg2, 3)
        
        
        all_switches = {}
        roles = {'t', 'd', 's'}
        
        for role in roles:
            players_team_1 = team1.getPlayers(role)
            players_team_2 = team2.getPlayers(role)
    
            for p1 in players_team_1:
                for p2 in players_team_2:
                    s = Switch(team1, role, p1, team2, role, p2)
                    change = s.getSRChange()
                    #print("Switch " + str(counter) + " added: " + s.toShortString())
                    # print("SR diff: " + str(round(change, 3)))
                    #counter += 1
                    #all_switches[change] = s
                    k = abs(diff + change)
                    if abs(k) <= abs(diff) and change != 0:
                        all_switches[k] = s
        
        switch_keys = list(all_switches.keys())
        
        if len(switch_keys) == 0:
            stillSwitchesPossible = False
            break
        
        print()
        print("Current SR difference (team1 - team2): " + str(diff))
        print("There are " + str(len(switch_keys)) + " switch(es) available:")
        #for tk in switch_keys:
            #print(str(round(tk, 3)) + ": " + all_switches[tk].toShortString())
        
        list.sort(switch_keys)
        
        #print()
        for tk in switch_keys:
            print(str(round(tk, 3)) + ": " + all_switches[tk].toShortString())
        
        best_switch = all_switches[switch_keys[0]]
        print("\nSwitching " + best_switch.toShortString())
        best_switch.makeSwitch()
        counter += 1
    
    
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    diff = round(avg1 - avg2, 3)
    
    print(str(counter) + " switch(es) done.")
    print("Starting difference: " + str(startdiff))
    print("Final SR difference (team1 - team2): " + str(diff) + "\n")


"""
This function is for filling any gaps as well as balancing out both teams
"""
def fill_loop():
    global all_teams
    
    # Get the ID of the teams. This indicates which players are on which team
    totalID = ""
    for t in all_teams:
        totalID += t.getID()
    
    oldID = ""
    
    
    # We can use this to see if there were any switches made or not
    # The counter is in the loop to prevent infinite switches which should theoretically not occur, I just got it in for safety
    counter = 0
    while oldID != totalID and counter < 20:
        fill_the_rest()
        balance_teams_1()
        
        oldID = totalID
        totalID = ""
        for t in all_teams:
            totalID += t.getID()
        counter += 1
    

"""
Central function
"""
def run():
    global all_players
    global final_message
    
    # If we want to output everything to a file, this flag needs to be true (set all the way on the top)
    
    # if outputTextFile:
    #     sys.stdout = open("output.txt", "w")
    
    print(datetime.now())
    
    #############
    
    # First, get all the players
    # getPlayers_no_pref_role() # Still TODO
    getPlayers_with_pref_role()
    
    # #############
    
    # Calculate the number of teams that can be made from all available players and create them
    number_of_teams = int(len(all_players)/6)
    createTeams(number_of_teams)
    
    # Fill the teams (1st round)
    fill_teams_1()
    
    # Fill the bench
    fill_bench()
    
    # Run the loop to fill any gaps
    fill_loop()
    
    # Print the teams (currently our output)
    printAllTeams()
    
    # Print the final message (matchups)
    print(final_message)
    
    # Don't mind this :D
    
    # try:
    print("I want to close pliz")
    # sys.stdout.close()
    # except:
    #     print("Closing didn't work?")
    
    print("I WANT TO EXIT PLZ")
    sys.exit()
    
    #############
        

if __name__== "__main__":
    print("Running")
    
    run()
    print("still not exited")
    sys.exit()