# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 19:33:15 2020

@author: Philipp von Perponcher
"""

import math
import random as rand


"""
Method createPlayers

Creates a list of imaginary players that can be used to fill teams.
The list is created in the following format:
    <Name> - <SR Tank> - <SR DPS> - <SR Support>

@param low: lowest possible SR
@param high: highest possible SR
@param number_players: number of players that are to be created
"""
def createPlayers(low, high, number_players):
    print("Creating players...")
    
    if low >= high:
        print("Error: low must be smaller than high")
        return
    
    # Defining the range
    limit_low = low + 300
    limit_high = high - 300
    
    # Creating the players by writing them into a file
    with open("players.txt", 'w') as f:
        f.write("# <Name> - <SR Tank> - <SR DPS> - <SR Support>\n")
        
        for player_number in range(1, number_players+1):
            
            # Writing the player's 'name'
            player = "Player " + str(player_number)
            
            # Creating the player's middle SR:
            mid_SR = rand.randint(limit_low, limit_high)
            player_low = mid_SR - 300
            player_high = mid_SR + 300
            
            # Creating the 3 different SR levels
            for i in range(3):
                sr = rand.randint(player_low, player_high)
                player += " - " + str(sr)
            
            # Writing the String into a file
            if player_number == number_players:
                f.write(player)
            else:
                f.write(player + "\n")
    
    print("Done")


if __name__== "__main__":
    createPlayers(1400, 3600, 40)