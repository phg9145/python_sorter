# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 19:33:15 2020

@author: Philipp von Perponcher
"""

import math
import random as rand


"""
Method createPlayers

Creates a list of imaginary players that can be used to fill teams.
The list is created in the following format:
    <Name> - <SR Tank> - <SR DPS> - <SR Support> - <Roles>

@param low: lowest possible SR
@param high: highest possible SR
@param number_players: number of players that are to be created
"""
def createPlayers(low, high, number_players):
    print("Creating players...")
    
    reg_num = (int)(number_players/3)
    number_tank = reg_num-2
    number_dps = reg_num-2
    number_supp = reg_num-2
    
    for i in range(number_players % 3):
        r = rand.randint(1,9)
        if r < 4:
            number_tank += 1
        elif r > 6:
            number_supp += 1
        else:
            number_dps += 1
    
    print("Number of players: " + str(number_players))
    print("Number of tanks: " + str(number_tank))
    print("Number of DPS: " + str(number_dps))
    print("Number of supports: " + str(number_supp))
    
    all_roles = []
    for i in range(number_tank):
        all_roles.append("t")
    for i in range(number_dps):
        all_roles.append("d")
    for i in range(number_supp):
        all_roles.append("s")
    
    # Addding flex roles
    roles = ('td', 'ds', 'ts', 'tds')
    
    for i in range(6):
        sel = str(rand.choice(roles))
        print("Flex: " + sel)
        all_roles.append(sel)
    
    rand.shuffle(all_roles)
    
    print(all_roles)
    
    
    if low >= high:
        print("Error: low must be smaller than high")
        return
    
    # Defining the range
    limit_low = low + 300
    limit_high = high - 300
    
    # Creating the players by writing them into a file
    with open("players.txt", 'w') as f:
        f.write("# <Name> - <SR Tank> - <SR DPS> - <SR Support> - <Roles>\n")
        
        for player_number in range(1, number_players+1):
            
            # Writing the player's 'name'
            player = "Player " + str(player_number)
            
            # Creating the player's middle SR:
            mid_SR = rand.randint(limit_low, limit_high)
            player_low = mid_SR - 300
            player_high = mid_SR + 300
            
            # Creating the 3 different SR levels
            for i in range(3):
                sr = rand.randint(player_low, player_high)
                player += " - " + str(sr)
            
            # Appending the role, that the player plays, to the end of the String
            player += " - " + all_roles.pop(0)
            
            # Writing the String into a file
            if player_number == number_players:
                f.write(player)
            else:
                f.write(player + "\n")
    
    print("Done")


if __name__== "__main__":
    createPlayers(1400, 3600, 17)