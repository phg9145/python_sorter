# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 20:17:15 2020

@author: Philipp von Perponcher
"""


from Player import Player
from Switch import Switch
from Team import Team
import random as rand


all_teams = []
all_players = []
available_players = []
final_message = ""



"""
The most basic function of this program: "Give" a player p to a team t in role r (r in {'t','d','s'})

@param t: Team
@param p: Player
@param r: Role (in {'t','d','s'})
"""
def pickPlayer(t, p, r):
    global available_players
    
    #print("Trying to pick player " + p.getName() + " to team " + t.getName() + " for role " + str(r) + ".....")
    
    if p not in available_players:
        print("Error: Player not available for picking")
        return False
    
    if r in ('t', 'd', 's'):
        if t.givePlayer(p, r) == True:
            available_players.remove(p)
            print("Pick successfull!")
            return True
        else:
            print("Error: Player can't be picked as a tank for this team (role full)")
            return False
    else:
        print("Error: " + str(r) + " is not a role!")
        return False



    

# Functions for good printing of the respective object

def printAllPlayers():
    global all_players
    
    print("#############")
    print("All players that are up for being picked:\n")
    
    for p in all_players:
        print(p.toString())
    
    print("#############")

def printAvailablePlayers():
    global available_players
    
    print("#############")
    print("All players that are still pickable:\n")
    
    for p in available_players:
        print(p.toString())
    
    print("#############")



def printAllTeams():
    global all_teams
    
    for t in all_teams:
        t.printTeam()



# Now, the players have to be read from the players.txt file and assigned to Teams


"""
Getting all players from the players.txt file and saving them into the global list all_players
"""
def getPlayers():
    global all_players
    global available_players
    
    with open("players.txt", 'r') as f:
        aL = f.readlines()
    
    allLines = []
    for line in aL:
        if line[0] != '#':
            allLines.append(line.rstrip())
    
    #for l in allLines:
        #print(l)
    
    # Read all lines of the players-file, now the lines need to be split and assigned to the players
    
    for line in allLines:
        line_split = line.split(' - ')
        
        allRoles = ['t','d','s']
        p = Player(line_split[0], int(line_split[1]), int(line_split[2]), int(line_split[3]), allRoles)
        available_players.append(p)
    
    # Now, all players should be saved in the global variable all_players.
    # Let's print all their names just to be sure
    
    #for p in available_players:
        #print(p.getName())
    
    all_players = available_players.copy()
    
    print("All players read!")
    print()

"""
Before filling them, the teams have to be created first

@param numberOfTeams: the number of teams to be created
"""
def createTeams(numberOfTeams):
    global all_teams
    
    for i in range(1,numberOfTeams+1):
        name = "Team " + str(i)
        t = Team(name)
        all_teams.append(t)


"""
Help Method of filling the teams: first 6 --> team1 etc.
"""
def fill_teams_0():
    global all_teams
    global available_players
    
    for t in all_teams:
        pickPlayer(t, available_players[0], 't')
        pickPlayer(t, available_players[0], 't')
        pickPlayer(t, available_players[0], 'd')
        pickPlayer(t, available_players[0], 'd')
        pickPlayer(t, available_players[0], 's')
        pickPlayer(t, available_players[0], 's')
    
    return 0
"""
1st Method of filling the teams: random
"""
def fill_teams_1():
    global all_teams
    global available_players
    
    for t in all_teams:
        tank_random = rand.sample(available_players, 2)
        for p in tank_random:
            pickPlayer(t, p, 't')
            
        dps_random = rand.sample(available_players, 2)
        for p in dps_random:
            pickPlayer(t, p, 'd')
            
        supp_random = rand.sample(available_players, 2)
        for p in supp_random:
            pickPlayer(t, p, 's')
    
    return 0


def balance_teams_1():
    global all_teams
    global final_message
    
    teams = {}
    for t in all_teams:
        teams[t.getAvgTotal()] = t
    
    team_keys = list(teams.keys())
    print(team_keys)
    list.sort(team_keys)
    print(team_keys)
    
    for i in range(int(len(teams)/2)):
        t1 = teams[team_keys.pop(0)]
        t2 = teams[team_keys.pop(0)]
        make_balance_1(t1, t2)
        make_balance_2(t1, t2)
        final_message += "Matchup " + str(i+1) + ": " + t1.getName() + " - " + t2.getName() + ", difference: " + str(round(t1.getAvgTotal() - t2.getAvgTotal(), 3)) + "\n"
    
    
    if len(team_keys) != 0:
        final_message += "Leftover team: " + teams[team_keys[0]].getName()


def make_balance_1(team1, team2):
    print("########################################")
    print("########################################")
    print("Balancing " + team1.getName() + " and " + team2.getName() + "...")
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    startdiff = round(avg1 - avg2, 3)
    
    counter = 0
    
    stillSwitchesPossible = True
    # Try tank switches
    while stillSwitchesPossible:
        avg1 = team1.getAvgTotal()
        avg2 = team2.getAvgTotal()
        diff = round(avg1 - avg2, 3)
        
        
        all_switches = {}
        roles = {'t', 'd', 's'}
        
        for role1 in roles:
            for role2 in roles:
                players_team_1 = team1.getPlayers(role1)
                players_team_2 = team2.getPlayers(role2)
        
                for p1 in players_team_1:
                    for p2 in players_team_2:
                        s = Switch(team1, role1, p1, team2, role2, p2)
                        change = s.getSRChange()
                        #print("Switch " + str(counter) + " added: " + s.toShortString())
                        # print("SR diff: " + str(round(change, 3)))
                        #counter += 1
                        #all_switches[change] = s
                        k = abs(diff + change)
                        if abs(k) <= abs (diff) and change != 0:
                            all_switches[k] = s
        
        switch_keys = list(all_switches.keys())
        
        if len(switch_keys) == 0:
            stillSwitchesPossible = False
            break
        
        print()
        print("Current SR difference (team1 - team2): " + str(diff))
        print("There are " + str(len(switch_keys)) + " switches available:")
        #for tk in switch_keys:
            #print(str(round(tk, 3)) + ": " + all_switches[tk].toShortString())
        
        list.sort(switch_keys)
        
        #print()
        for tk in switch_keys:
            print(str(round(tk, 3)) + ": " + all_switches[tk].toShortString())
        
        best_switch = all_switches[switch_keys[0]]
        print("\nSwitching " + best_switch.toShortString())
        best_switch.makeSwitch()
        counter += 1
    
    
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    diff = round(avg1 - avg2, 3)
    
    print(str(counter) + " switch(es) done.")
    print("Starting difference: " + str(startdiff))
    print("Final SR difference (team1 - team2): " + str(diff) + "\n")


 
"""
Balancing only within the role
--> a tank player on team 1 can't be switched with a DPS on team 2
"""
def make_balance_2(team1, team2):
    print("########################################")
    print("########################################")
    print("Balancing " + team1.getName() + " and " + team2.getName() + "...")
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    startdiff = round(avg1 - avg2, 3)
    
    counter = 0
    
    stillSwitchesPossible = True
    # Try tank switches
    while stillSwitchesPossible:
        avg1 = team1.getAvgTotal()
        avg2 = team2.getAvgTotal()
        diff = round(avg1 - avg2, 3)
        
        
        all_switches = {}
        roles = {'t', 'd', 's'}
        
        for role in roles:
            players_team_1 = team1.getPlayers(role)
            players_team_2 = team2.getPlayers(role)
    
            for p1 in players_team_1:
                for p2 in players_team_2:
                    s = Switch(team1, role, p1, team2, role, p2)
                    change = s.getSRChange()
                    #print("Switch " + str(counter) + " added: " + s.toShortString())
                    # print("SR diff: " + str(round(change, 3)))
                    #counter += 1
                    #all_switches[change] = s
                    k = abs(diff + change)
                    if abs(k) <= abs (diff) and change != 0:
                        all_switches[k] = s
        
        switch_keys = list(all_switches.keys())
        
        if len(switch_keys) == 0:
            stillSwitchesPossible = False
            break
        
        print()
        print("Current SR difference (team1 - team2): " + str(diff))
        print("There are " + str(len(switch_keys)) + " switches available:")
        #for tk in switch_keys:
            #print(str(round(tk, 3)) + ": " + all_switches[tk].toShortString())
        
        list.sort(switch_keys)
        
        #print()
        for tk in switch_keys:
            print(str(round(tk, 3)) + ": " + all_switches[tk].toShortString())
        
        best_switch = all_switches[switch_keys[0]]
        print("\nSwitching " + best_switch.toShortString())
        best_switch.makeSwitch()
        counter += 1
    
    
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    diff = round(avg1 - avg2, 3)
    
    print(str(counter) + " switch(es) done.")
    print("Starting difference: " + str(startdiff))
    print("Final SR difference (team1 - team2): " + str(diff) + "\n")

def switch_try():
    global all_teams
    team1 = all_teams[-1]
    team2 = all_teams[-2]
    
    
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    diff = avg1 - avg2
    print("SR difference before Switch (team1 - team2): " + str(diff))
    
    role1 = role2 = 't'
    
    players_t1_r1 = team1.getPlayers(role1)
    players_t2_r2 = team2.getPlayers(role2)
    
    s = Switch(team1, role1, players_t1_r1[0], team2, role2, players_t2_r2[0])
    
    s.makeSwitch()
    
    
    avg1 = team1.getAvgTotal()
    avg2 = team2.getAvgTotal()
    diff = avg1 - avg2
    print("SR difference after Switch (team1 - team2): " + str(diff))
    
    #switchPlayers()

def run():
    global all_players
    global available_players
    global all_teams
    
    global final_message
    
    #############
    
    getPlayers()
    
    #printAllPlayers()
    #printAvailablePlayers()
    
    #############
    
    number_of_teams = int(len(all_players)/6)
    createTeams(number_of_teams)
    
    #printAllTeams()
    
    fill_teams_1()
    
    #printAvailablePlayers()
    printAllTeams()
    
    #balance_teams_1(all_teams[0], all_teams[1])
    balance_teams_1()
    #switch_try()
    
    printAllTeams()
    
    print(final_message)
    
    #############
        

if __name__== "__main__":
    
    run()