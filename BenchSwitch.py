# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 17:09:45 2020

@author: Philipp
"""



class BenchSwitch:
    
    def __init__(self, team, role, player1, bench, player_bench):
        self.team = team
        self.bench = bench
        self.role = role
        self.player1 = player1
        self.player_bench = player_bench
        
        self.calcSRChange()
        
        self.roleName = ''
        if role == 't':
            self.roleName = 'Tank'
        elif role == 'd':
            self.roleName = 'DPS'
        elif role == 's':
            self.roleName = 'Support'
        else:
            self.roleName = 'Error with Role 1'
        
        
        self.calcSwitchPossible()
        #print("switchPossible was calculated, it's " + str(self.switchPossible))
    
    
    def getID(self):
        val_str = ""
        plList = [self.player1.getName(), self.player_bench.getName()]
        # print(plList)
        plList.sort()
        # print(plList)
        for p in plList:
            val_str += p
        
        val_str += self.role
        val_str += "b"
        
        return val_str
    
    
    def getSRChange(self):
        return self.SRChange
    
    def getSwitchPossible(self):
        return self.switchPossible
    """
    Function to calculate, which SR difference it would make if this switch was made
    @return: returns the difference of the new average SRs.
    Example: A switch, where team gets +10SR and team2 -15SR returns +25
    """
        
    def calcSRChange(self):
        
        p1SR = self.player1.getSR(self.role)
        p2SR = self.player_bench.getSR(self.role)
        
#        team_without_p1 = 0 - p1SR
#        team_with_bPlayer = team_without_p1 + p2SR
        
        difference = p2SR - p1SR
        
        self.SRChange = difference / 6

    def makeSwitch(self):
        print("In makeSwitch Bench")
        switch_1_success = self.team.removePlayer(self.player1, self.role)
        switch_2_success = self.bench.removePlayerNoRole(self.player_bench)
        
        print("switch_1_success: " + str(switch_1_success))
        print("switch_2_success: " + str(switch_2_success))
        
        if switch_1_success == False:
            self.team.givePlayer(self.player1, self.role)
        
        if switch_2_success == False:
            self.bench.givePlayer(self.player_bench, self.role)
        
        if switch_1_success and switch_2_success:
            self.team.givePlayer(self.player_bench, self.role)
            self.bench.givePlayer(self.player1, self.role)
        else:
            print("AND-operation not successful")
            return False
        
        print()
        print("Switch was made:")
        print(self.player1.getName() + " was transfered to the Bench")
        print(self.player_bench.getName() + " was transfered to " + self.team.getName() + ", new Role: " + self.roleName)
        print("With this Bench Switch, " + str(self.team.getName()) + " got an SR Change by " + str(round(self.SRChange, 3)))
        print()
        return True
    
    def toShortString(self):
        return self.player1.getName() + " <--> " + self.player_bench.getName() + " (Bench)"

    def calcSwitchPossible(self):
# =============================================================================
#         print("-----")
#         print("In a switch")
#         print("Role 1: " + self.roleName)
#         print("Role 2: " + self.roleName)
#         print("Player 1: " + self.player1.toString())
#         print("Player 2: " + self.player_bench.toString())
#         print()
#         print("Player 2 roles: " + str(self.player_bench.getRoles()))
#         print("Role 1: " + self.role)
#         print("Role 1 in player 2's roles? " + str(self.role in self.player_bench.getRoles()))
#         print()
#         print("Player 1 roles: " + str(self.player1.getRoles()))
#         print("Role 2: " + self.role)
#         print("Role 2 in player 1's roles? " + str(self.role in self.player1.getRoles()))
# =============================================================================
        self.switchPossible = (self.role in self.player_bench.getRoles()) & (self.role in self.player1.getRoles())
        
#        print("Switch possible? " + str(self.switchPossible))
