# -*- coding: utf-8 -*-
"""
Created on Sat Aug  1 20:36:21 2020

@author: Philipp
"""

import statistics as stat

class Team:
    
    def __init__(self, name):
        self.name = name
        self.players_tank = []
        self.players_dps = []
        self.players_supp = []
    
    
    def getID(self):
        val_str = ""
        val_str += self.name
        for pl in self.getAllPlayers():
            val_str += pl.getName()
        
        return val_str
    
    def getName(self):
        return self.name
    
    def getNumberTank(self):
        return len(self.players_tank)
    
    def getNumberDPS(self):
        return len(self.players_dps)
    
    def getNumberSupp(self):
        return len(self.players_supp)
    
    def getNumberRole(self, role):
        if role == 't':
            return self.getNumberTank()
        
        elif role == 'd':
            return self.getNumberDPS()
        
        elif role == 's':
            return self.getNumberSupp()
        else:
            print("Error in getNumberRole: " + str(role) + " is not a role!")
            return -1
    
    def getPlayersTank(self):
        return self.players_tank
    
    def getPlayersDPS(self):
        return self.players_dps
    
    def getPlayersSupp(self):
        return self.players_supp
    
    def getAllPlayers(self):
        full = self.players_tank.copy()
        full.extend(self.players_dps)
        full.extend(self.players_supp)
        return full
    
    def getPlayers(self, role):
        if role == 't':
            return self.getPlayersTank()
        
        elif role == 'd':
            return self.getPlayersDPS()
        
        elif role == 's':
            return self.getPlayersSupp()
        else:
            print("Error in getPlayers: " + str(role) + " is not a role!")
            return False
    
    def getAvgTank(self):
        val = []
        
        # If no tanks are set atm, the function will return nothing
        if len(self.players_tank) == 0:
            return
        
        for p in self.players_tank:
            val.append(p.getTank())
        
        return stat.mean(val)
    
    def getAvgDPS(self):
        val = []
        
        # If no dps are set atm, the function will return nothing
        if len(self.players_dps) == 0:
            return
        
        for p in self.players_dps:
            val.append(p.getDPS())
        
        return stat.mean(val)
    
    def getAvgSupp(self):
        val = []
        
        # If no supports are set atm, the function will return nothing
        if len(self.players_supp) == 0:
            return
        
        for p in self.players_supp:
            val.append(p.getSupp())
        
        return stat.mean(val)
    
    def getAvgTotal(self):
        val = []
        
        # If no supports are set atm, the function will return nothing
        if len(self.players_tank) == 0 and len(self.players_dps) == 0 and len(self.players_supp) == 0:
            return -1
        
        for p in self.players_tank:
            val.append(p.getTank())
        
        for p in self.players_dps:
            val.append(p.getDPS())
        
        for p in self.players_supp:
            val.append(p.getSupp())
        
        return stat.mean(val)
    
    def getDeviation(self):
        val = []
        
        # If no supports are set atm, the function will return nothing
        if len(self.players_tank) == 0 and len(self.players_dps) == 0 and len(self.players_supp) == 0:
            return -1
        
        for p in self.players_tank:
            val.append(p.getTank())
        
        for p in self.players_dps:
            val.append(p.getDPS())
        
        for p in self.players_supp:
            val.append(p.getSupp())
        
        return stat.stdev(val)
        
    
    def giveTank(self, pl):
        if len(self.players_tank) < 2:
            self.players_tank.append(pl)
            return True
        else:
            print("Tank on team " + self.name + " is full:")
            for pl in self.players_tank:
                print(pl.toString())
            return False
    
    def giveDPS(self, pl):
        if len(self.players_dps) < 2:
            self.players_dps.append(pl)
            return True
        else:
            return False
    
    def giveSupp(self, pl):
        if len(self.players_supp) < 2:
            self.players_supp.append(pl)
            return True
        else:
            return False
    
    def givePlayer(self, player, role):
        if role == 't':
            return self.giveTank(player)
        
        elif role == 'd':
            return self.giveDPS(player)
        
        elif role == 's':
            return self.giveSupp(player)
        else:
            print("Error in givePlayer: " + str(role) + " is not a role!")
            return False
    
        
    
    def removePlayer(self, player, role):
        if role == 't':
            try:
                self.players_tank.remove(player)
            except:
                return False
        
        elif role == 'd':
            try:
                self.players_dps.remove(player)
            except:
                return False
        elif role == 's':
            try:
                self.players_supp.remove(player)
            except:
                return False
        else:
            print("Error: " + str(role) + " is not a role!")
            return False
        
        return True

    def printTeam(self):
        print("-------------")
        print("Team Name: " + self.name)
        print("Team average: " + str(round(self.getAvgTotal(), 3)))
        print("Standard deviation: " + str(round(self.getDeviation(), 3)))
        print()
        
        for x in range(2):
            if len(self.players_tank) > x:
                p = self.players_tank[x]
                st = p.getName() + " - " + str(p.getTank())
            else:
                st = " - "
            print("Tank: " + st)
            
        for x in range(2):
            if len(self.players_dps) > x:
                p = self.players_dps[x]
                st = p.getName() + " - " + str(p.getDPS())
            else:
                st = " - "
            print("DPS: " + st)
            
        for x in range(2):
            if len(self.players_supp) > x:
                p = self.players_supp[x]
                st = p.getName() + " - " + str(p.getSupp())
            else:
                st = " - "
            print("Support: " + st)
        
        print("-------------")


    def printTeamShort(self):
        print("-------------")
        print("Team Name: " + self.name)
        print("Team average: " + str(round(self.getAvgTotal(), 3)))
        print("Standard deviation: " + str(round(self.getDeviation(), 3)))
    
    def getGaps(self):
        st = ""
        if self.getNumberDPS() != 2:
            st += "d"
        if self.getNumberTank() != 2:
            st += "t"
        if self.getNumberSupp() != 2:
            st += "s"
        
        return st
    
    def movePlayer(self, player, role_old, role_new):
        if self.getNumberRole(role_new) < 2:
            if self.removePlayer(player, role_old) == True:
                self.givePlayer(player, role_new)
                return True
        
        return False
            